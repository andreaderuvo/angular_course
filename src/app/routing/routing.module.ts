import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from '../pages/welcome/welcome.component';
import { LoginComponent } from '../pages/login/login.component';
import { AboutComponent } from '../pages/about/about.component';
import { NotFoundComponent } from '../layout/notfound/notfound.component';
import { TodoNewComponent } from '../pages/todo/todo-new/todo-new.component';
import { TodoComponent } from '../pages/todo/todo/todo.component';
import { IsLoggedInGuard } from '../guards/is-logged-in.guard';
import { AdminComponent } from '../pages/admin/admin.component';
import { UnauthorizedComponent } from '../layout/unauthorized/unauthorized.component';
import { HasRoleGuard } from '../guards/has-role.guard';

const routes: Routes = [{
  path: '',
  redirectTo: 'welcome',
  pathMatch: 'full'
},
{
  path: '',
  component: WelcomeComponent
},
{
  path: 'login',
  component: LoginComponent
},
{
  path: 'todo/new',
  component: TodoNewComponent,
  canActivate: [IsLoggedInGuard]
},
{
  path: '401',
  component: UnauthorizedComponent,
  canActivate: [IsLoggedInGuard]
},
{
  path: 'admin',
  component: AdminComponent,
  canActivate: [IsLoggedInGuard, HasRoleGuard],
  data: {
    role: 'ADMIN'
  }
},
{
  path: 'todo',
  component: TodoComponent,
  canActivate: [IsLoggedInGuard]
},
{
  path: 'about',
  component: AboutComponent
},
{
  path: '404',
  component: NotFoundComponent
},
{
  path: '**',
  pathMatch: 'full',
  redirectTo: '/404'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule { }
