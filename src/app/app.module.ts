//angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

//components
import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { LoginComponent } from './pages/login/login.component';
import { AboutComponent } from './pages/about/about.component';
import { NotFoundComponent } from './layout/notfound/notfound.component';
import { TodoNewComponent } from './pages/todo/todo-new/todo-new.component';
import { TodoComponent } from './pages/todo/todo/todo.component';

//routing
import { RoutingModule } from './routing/routing.module';
import { LoginService } from './pages/login/login.service';
import { IsLoggedInDirective } from './directives/is-logged-in.directive';
import { IsLoggedInGuard } from './guards/is-logged-in.guard';
import { HasRoleDirective } from './directives/has-role.directive';
import { AdminComponent } from './pages/admin/admin.component';
import { UnauthorizedComponent } from './layout/unauthorized/unauthorized.component';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    WelcomeComponent,
    LoginComponent,
    NotFoundComponent,
    AboutComponent,
    TodoNewComponent,
    TodoComponent,
    IsLoggedInDirective,
    HasRoleDirective,
    AdminComponent,
    UnauthorizedComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    RoutingModule
  ],
  providers: [LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
