import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Todo } from '../pages/todo/todo';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(protected httpClient : HttpClient) { }

  readUsers() {
    return this.httpClient.get(environment.read_users_service);
  }

  createTodo(todo : Todo) {
    return this.httpClient.post(environment.create_post_service, todo);
  }
}
