import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { LoginService } from '../pages/login/login.service';

@Directive({
  selector: '[isLoggedIn]'
})
export class IsLoggedInDirective implements OnInit {

  @Input("isLoggedIn") condition: boolean;

  constructor(protected loginService: LoginService,
    protected templateRef: TemplateRef<any>, protected viewContainer: ViewContainerRef) {

    this.loginService.userSubject.subscribe((user) => {
      this.updateView();
    })
  }

  ngOnInit(): void {
    this.updateView();
  }

  updateView(): void {
    this.viewContainer.clear();
    if (this.loginService.isLoggedIn() == this.condition) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

}
