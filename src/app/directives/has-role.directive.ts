import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { LoginService } from '../pages/login/login.service';

@Directive({
  selector: '[hasRole]'
})
export class HasRoleDirective {

  @Input("hasRole") role: string;

  constructor(protected loginService: LoginService,
    protected templateRef: TemplateRef<any>, protected viewContainer: ViewContainerRef) {

    this.loginService.userSubject.subscribe((user) => {
      this.updateView();
    })
  }

  ngOnInit(): void {
    this.updateView();
  }

  updateView(): void {
    this.viewContainer.clear();
    if (this.loginService.isLoggedIn() && this.loginService.hasRole(this.role)) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

}
