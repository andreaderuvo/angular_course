import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;

  constructor(protected router: Router, protected loginService: LoginService, protected formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      "email": new FormControl("", [Validators.required, Validators.email]),
      "password": new FormControl("", [Validators.required])
    });
  }

  submit(event) {
    if (this.loginForm.invalid) {
      return;
    } else {
      this.submitted = true;
      if (this.loginService.login({
        email: this.f.email.value,
        password: this.f.password.value
      })) {
        this.router.navigate(['/todo']);
      } else {
        alert("KO");
      }
    }
  }

  reset(event) {
    event.preventDefault();
    this.loginForm.reset();
  }

  get f() {
    return this.loginForm.controls;
  }

}
