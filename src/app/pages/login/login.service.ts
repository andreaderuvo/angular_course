import { Injectable } from '@angular/core';
import { Login } from './login';
import { USERS } from '../../db';
import { BehaviorSubject } from 'rxjs';

declare let $: any;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  static LOCALSTORATE_LOGIN_KEY = 'login';
  userSubject = new BehaviorSubject<any>({});

  constructor() { }

  login(login: Login): boolean {
    let user = USERS.find((user) => user.email.toLowerCase() === login.email.toLowerCase() && user.password === login.password);
    if (user) {
      let cloneUser = $.extend(true, {}, user);
      delete cloneUser.password;
      localStorage.setItem(LoginService.LOCALSTORATE_LOGIN_KEY, JSON.stringify(cloneUser));
      this.userSubject.next(cloneUser);
      return true;
    }
    this.logout();
    return false;
  }

  logout() {
    localStorage.removeItem(LoginService.LOCALSTORATE_LOGIN_KEY);
    this.userSubject.next(null);
  }

  isLoggedIn() {
    return (localStorage.getItem(LoginService.LOCALSTORATE_LOGIN_KEY)) ? true : false;
  }

  hasRole(role: string) {
    if (this.isLoggedIn()) {
      return this.user.role === role;
    } {
      return false;
    }
  }

  get user() {
    return JSON.parse(localStorage.getItem(LoginService.LOCALSTORATE_LOGIN_KEY));
  }
}
