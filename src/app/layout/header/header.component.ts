import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { LoginService } from '../../pages/login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  constructor(private router: Router, private loginService: LoginService) { }

  ngOnInit() {
  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['']);
  }

  get user() {
    return this.loginService.user;
  }

}
