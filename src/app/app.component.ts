import { Component } from '@angular/core';
import { environment } from '../environments/environment';
import { RestService } from './rest/rest.service';
import { Todo } from './pages/todo/todo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Corso Angular @ UNIVAQ';

  get context() {
    return environment.production ? 'PRODUCTION' : 'DEVELOPMENT';
  }

  constructor(protected restService: RestService) {
    restService.readUsers().subscribe((data) => console.log(data));

    let todo : Todo = new Todo();
    todo.userId = 1;
    todo.completed = true;
    todo.title = "Titolo";

    restService.createTodo(todo).subscribe((data) => console.log(data));

  }
}
